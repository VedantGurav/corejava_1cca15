package ArrayPattern;

public class Array5 {
    public static void main(String[] args) {
        int[] arr={1,5,2,4,7,9};
        int max=arr[0];
        for(int a:arr){
            if(a>max){
                max=a;
            }
        }
        System.out.println("THE MAX NO. IS:" +max);
    }
}
