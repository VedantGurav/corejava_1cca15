package ArrayPattern;

public class ArrayReverse {
    public static void main(String[] args) {
        int[]arr={1,2,3,4,5};
        int count= arr.length-1;
        for (int a:arr){
            System.out.print(a+"  ");
        }
        for (int i=0;i< arr.length/2;i++){
            int temp=arr[i];
            arr[i]=arr[count];
            arr[count]=temp;
            count--;
        }
        System.out.println();
        for (int a:arr){
            System.out.print(a+"  ");
        }
    }
}
