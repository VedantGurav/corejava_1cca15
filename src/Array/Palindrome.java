package Array;

public class Palindrome {
    public static void main(String[] args) {
        for (int i = 1; i < 10000; i++) {
            int a = i;
            int sum = 0;
            int temp = a;
            while (a != 0) {
                int r = a % 10;
                sum = sum * 10 + r;
                a /= 10;
            }
            if (sum == temp) {
                System.out.println("THE NUMBER IS PALINDROME" + sum);
            }
        }
    }
}
