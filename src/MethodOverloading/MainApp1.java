package MethodOverloading;

import java.util.Scanner;

public class MainApp1 {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        Student sc1=new Student();
        System.out.println("SELECT SEARCH CRITERIA");
        System.out.println("1:SEARCH BY NAME");
        System.out.println("2.SEARCH BY CONTACT");
        int ch=sc.nextInt();
        if(ch==1){
            System.out.println("ENTER NAME");
            String name=sc.next();
            sc1.search(name);
        } else if (ch==2) {
            System.out.println("ENTER CONTACT");
            int Contact= sc.nextInt();
            sc1.search(Contact);

        }
        else{
            System.out.println("INVALID CHOICE");
        }

    }
}
