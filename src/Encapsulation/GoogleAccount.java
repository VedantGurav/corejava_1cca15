package Encapsulation;
//singleton class

public class GoogleAccount {
    //static int count;
    static GoogleAccount g1;
    private GoogleAccount(){

    }
    //singleton method
    static GoogleAccount login(){
        if(g1==null){
            //singleton object
            g1=new GoogleAccount();
            System.out.println("LOGIN SUCESSFUL");
        }else{
            System.out.println("ALREADY LOGGEDIN");
        }
        return g1;
    }
    void accessGmail(){
        System.out.println("ACCESSING GMAIL");
    }
    void accessDrive(){
        System.out.println("ACCESSING DRIVE");
    }
}
