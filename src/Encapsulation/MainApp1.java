package Encapsulation;

public class MainApp1 {
    public static void main(String[] args) {
        Employee e1=new Employee();
        int id= e1.getEmpId();
        double salary= e1.getEmpSalary();
        System.out.println("ID: "+id);
        System.out.println("SALARY: "+salary);
        e1.setEmpId(201);
        e1.setEmpSalary(-35000);
        System.out.println(e1.getEmpSalary());
        System.out.println(e1.getEmpId());
    }
}
