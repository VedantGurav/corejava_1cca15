package Encapsulation;

public class Employee {
    private int empId=101;
    private double empSalary=12000;
    public int getEmpId(){
    return empId;
    }
    public void setEmpId(int empId){
        this.empId=empId;
    }
    public double getEmpSalary(){
        return empSalary;
    }
    public void setEmpSalary(double empSalary){
        if(empSalary>0){
            this.empSalary=empSalary;
        }else {
            System.out.println("INVALID AMOUNT");
        }
    }
}