package AbstractClass;

public class Manager extends Employee {
    @Override
    void getDesignation() {
        System.out.println("DESIGNATION IS MANAGER");
    }
    @Override
    void getSalary(){
        System.out.println("SALARY IS 150000");
    }
}

