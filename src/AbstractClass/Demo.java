package AbstractClass;

public abstract class Demo {
    static int k=20;
    double d=35.25;
    abstract void test();
    void disply(){
        System.out.println("DISPLY METOD");
    }
    static void info(){
        System.out.println("INFO METHOD");
    }
    Demo(){
        System.out.println("CONSTRUCTOR");
    }
    static{
        System.out.println("STATIC BLOCK");
    }
    {
        System.out.println("NON-STATIC");
    }
}
