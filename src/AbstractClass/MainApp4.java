package AbstractClass;

import java.util.Scanner;

public class MainApp4 {
    public static void main(String[] args) {
        Scanner sc1=new Scanner(System.in);
        System.out.println("SELECT TYPE");
        System.out.println("1.MANAGER\n2:WATCHMAN");
        int choice= sc1.nextInt();
        Employee e=null;
        if(choice==1){
            e=new Manager();
        } else if (choice==2) {
            e=new Watchman();
        }
        e.getDesignation();
        e.getSalary();
    }
}
