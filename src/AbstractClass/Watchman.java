package AbstractClass;

public class Watchman extends Employee{
    @Override
    void getDesignation(){
        System.out.println("DESIGNATION IS SR.WATCHMAN");
    }
    @Override
    void getSalary(){
        System.out.println("SALARY IS 400000");
    }
}
