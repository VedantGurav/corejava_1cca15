package Interfaces;

public class MainApp2 {
    public static void main(String[] args) {
        CreditCard credit;
        credit=new Visa();
        credit.getType();
        credit.withdraw(2500);
        System.out.println("==================");
        credit=new MasterCard();
        credit.withdraw(35);
    }
}
