package Interfaces;

public class MainApp4 {
    public static void main(String[] args) {
        Software s1=new Software();
        s1.designUI();
        s1.developServerProgram("PYTHON");
        s1.designDatabase("ORACLE");
    }
}
