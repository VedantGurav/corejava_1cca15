package Interfaces;

public class Software extends FrontEnd implements BackEnd,Database {

    @Override
    public void developServerProgram(String language) {
        System.out.println("DEVELOPING SERVER PROGRAMING USING: " + language);
    }

    @Override
    public void designDatabase(String dbVendor) {
        System.out.println("DESIGN DATABASE USING DATABASE SERVER: " + dbVendor);
    }
}

