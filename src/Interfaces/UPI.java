package Interfaces;
@FunctionalInterface
public interface UPI {
    void transferAmount(double amt);
}
